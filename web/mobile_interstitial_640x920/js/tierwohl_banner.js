var tierwohl_banner = new Object({
    ms: 1750,
    init: function () {
        $('.kreis').each(function (index) {
            $(this).delay(index * tb.ms).animate({
                top: 200 + index * 25 + index * 250 + 'px',
                left: '500px',
                width: '250px',
                height: '250px',
                opacity: 1
            }, tb.ms, function () {
                if (index === 2) {
                    $('#head1').delay(1000).transition({
                        x: '+=550'
                    }, tb.ms, function () {
                        $('#head2').delay(1500).transition({
                            x: '+=550'
                        }, tb.ms, function () {
                            $('#footer').delay(1500).transition({
                                y: '-=416px'
                            }, tb.ms);
                        });
                    });
                }
            });
        });

    }
});

var $ = jQuery;
var tb = tierwohl_banner;

$(document).ready(function () {

});
$(window).resize(function () {

});
$(window).load(function () {
    tb.init();
});