var tierwohl_banner = new Object({
    ms: 1250,
    init: function () {
        $('.kreis').each(function (index) {
            $(this).delay(index * tb.ms).animate({
                top: '35px',
                left: 5*index+ 70*index+'px',
                width: '70px',
                height: '70px',
                opacity: 1
            }, tb.ms);
        });
        $('#head1').delay(3*tb.ms).transition({
            x: '+=300'
        }, tb.ms);
        $('#head2').delay(4*tb.ms+500).transition({
            x: '+=300'
        }, tb.ms);
        $('#footer').delay(5*tb.ms+1000).transition({
            y: '-=77px'
        }, tb.ms);
    }
});

var $ = jQuery;
var tb = tierwohl_banner;

$(document).ready(function () {

});
$(window).resize(function () {

});
$(window).load(function () {
    tb.init();
});