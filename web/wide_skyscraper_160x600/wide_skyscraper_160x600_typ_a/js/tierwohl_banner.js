var tierwohl_banner = new Object({
    ms: 1250,
    init: function () {
        $('.kreis').each(function (index) {
            $(this).delay(index * tb.ms).animate({
                top: index*100+5*index+'px',
                left:'20px',
                width: '100px',
                height: '100px',
                opacity: 1
            }, tb.ms);
        });
        $('#head1').delay(3*tb.ms).transition({
            x: '+=300'
        }, tb.ms);
        $('#head2').delay(750+4*tb.ms).transition({
            x: '+=300'
        }, tb.ms);
        $('#footer').delay(750+750+5*tb.ms).transition({
            y: '-=145px'
        }, tb.ms);
    }
});

var $ = jQuery;
var tb = tierwohl_banner;

$(document).ready(function () {

});
$(window).resize(function () {

});
$(window).load(function () {
    tb.init();
});