var tierwohl_banner = new Object({
    ms: 1000,
    init: function () {
        $('#kreis').animate({
            top: '180px',
            left: '10px',
            width: '250px',
            height: '250px',
            opacity: 1
        }, tb.ms);
        $('#head1').delay(1*tb.ms).transition({
            x: '+=300'
        }, tb.ms);
        $('#head2').delay(750+2*tb.ms).transition({
            x: '+=300'
        }, tb.ms);
        $('#footer').delay(1250+750+3*tb.ms).transition({
            y: '-=145px'
        }, tb.ms);
    }
});

var $ = jQuery;
var tb = tierwohl_banner;

$(document).ready(function () {

});
$(window).resize(function () {

});
$(window).load(function () {
    tb.init();
});