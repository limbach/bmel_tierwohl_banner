var tierwohl_banner = new Object({
    ms: 1250,
    init: function () {
        $('.kreis').each(function (index) {
            $(this).delay(index * tb.ms).animate({
                top: 5*index+ 120*index+'px',
                left: '0px',
                width: '120px',
                height: '120px',
                opacity: 1
            }, tb.ms);
        });
        $('#head2').delay(4*tb.ms).transition({
            x: '-=300'
        }, tb.ms);
        $('#footer').delay(5*tb.ms).transition({
            y: '-=145px'
        }, tb.ms);
    }
});

var $ = jQuery;
var tb = tierwohl_banner;

$(document).ready(function () {

});
$(window).resize(function () {

});
$(window).load(function () {
    tb.init();
});