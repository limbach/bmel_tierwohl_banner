var tierwohl_banner = new Object({
    ms: 1500,
    init: function () {
        $('.kreis').each(function (index) {
            $(this).delay(index * tb.ms / 2).animate({
                top: '0px',
                left: 25 * index + 70 * index + 'px',
                width: '70px',
                height: '70px',
                opacity: 1
            }, tb.ms / 2, function () {
                if (index === 2) {
                    $('#kreise').delay(tb.ms/3).transition({
                        x: '+=320'
                    }, tb.ms);
                    $('#head1').delay(tb.ms/3).transition({
                        x: '+=320'
                    }, tb.ms, function () {
                        $('#head1').delay(tb.ms/3).transition({
                            x: '+=320'
                        }, tb.ms);
                        $('#head2').delay(tb.ms/3).transition({
                            x: '+=320'
                        }, tb.ms, function () {
                            $('#footer').delay(tb.ms/3).transition({
                                x: '+=320px'
                            }, tb.ms);
                        });
                    });
                }
            });
        });
    }
});

var $ = jQuery;
var tb = tierwohl_banner;

$(document).ready(function () {

});
$(window).resize(function () {

});
$(window).load(function () {
    tb.init();
});