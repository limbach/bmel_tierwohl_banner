var tierwohl_banner = new Object({
    ms: 1200,
    init: function() {
        $('.kreis').each(function(index) {
            $(this).delay(index * tb.ms / 3).animate({
                top: index * 25 + index * 250 + 'px',
                left: '500px',
                width: '250px',
                height: '250px',
                opacity: 1
            }, tb.ms / 2, function() {
                if (index === 2) {
                    $('#head1').delay(tb.ms / 4).transition({
                        x: '+=550'
                    }, tb.ms / 2, function() {
                        $('#head2').delay(tb.ms / 4).transition({
                            x: '+=550'
                        }, tb.ms / 2, function() {
                            $('#footer').delay(tb.ms / 4).transition({
                                y: '-=416px'
                            }, tb.ms / 2);
                        });
                    });
                }
            });
        });
    }
});
var $ = jQuery;
var tb = tierwohl_banner;
$(window).load(function() {
    tb.init();
});